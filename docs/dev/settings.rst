Project settings
================

This only lists settings that have docstrings. See the relevant
settings files for the complete settings, or run::

    $ python manage.py diffsettings

Base settings
-------------

.. automodule:: ycombinator_web.settings.base
   :members:

Deploy settings
---------------

.. automodule:: ycombinator_web.settings.deploy
   :members:

Development settings
--------------------

.. automodule:: ycombinator_web.settings.dev
   :members:
