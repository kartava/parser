from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

from ycombinator_web.api import views
from ycombinator_web.api.posts_views import PostViewSet

router = SimpleRouter()
router.register(r'posts', PostViewSet, base_name='posts')

urlpatterns = [
    url(r'^$', views.api_tree),
    url(r'^', include(router.urls)),
]
