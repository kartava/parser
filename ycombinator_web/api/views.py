# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict
from functools import partial

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse


def api_tree(request, format=None):
    """
    API Tree
    """
    r = partial(reverse, request=request, format=format)
    return Response(OrderedDict([
        ('posts', OrderedDict([
            ('list', r('api:posts-list')),
            ('detail', r('api:posts-detail', args=[1]))
        ])),
        ('api-auth', OrderedDict([
            ('login', r('rest_framework:login')),
            ('logout', r('rest_framework:logout')),
        ])),
    ]))


api_tree.permission_classes = ()
api_tree = api_view(('GET',))(api_tree)

